module.exports = {
  development: {
    dialect: "sqlite",
    storage: "./db.development.sqlite"
  },
  test: {
    dialect: "sqlite",
    storage: ":memory:"
  },
  dev: {
    username: "traineeUser",
    password: "traineePassword",
    database: "postgres",
    host: "traineedb.cgq0reqixqsd.us-east-1.rds.amazonaws.com",
    dialect: 'postgres'
  },
  production: {
    username: "traineeUser",
    password: "traineePassword",
    database: "postgres",
    host: "traineedb.cgq0reqixqsd.us-east-1.rds.amazonaws.com",
    dialect: 'postgres'
  }

};
